﻿using E1.Cardio.Trainer.Libs.Models.AppModels;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace E1.Cardio.Trainer.APIs.Middlewares
{
    public class JwtMiddlewares
    {
        private readonly RequestDelegate _next;
        private readonly Jwt _jwtSection;

        public JwtMiddlewares(RequestDelegate next, Jwt jwtSection)
        {
            _next = next;
            _jwtSection = jwtSection;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Path.StartsWithSegments("/login")
                || context.Request.Path.StartsWithSegments("/new_customer")
                || context.Request.Path.StartsWithSegments("/new_trainer"))
            {
                await _next(context);
                return;
            }

            if (!context.Request.Headers.ContainsKey("Authorization"))
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await context.Response.WriteAsync("Unauthorized");
                return;
            }

            try
            {
                var authHeader = context.Request.Headers["Authorization"].ToString().Split(" ");
                var claimsPrincipal = GetClaimPrincipal(authHeader[1]);
                context.User = claimsPrincipal;
            }
            catch (Exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await context.Response.WriteAsync("Unauthorized");
                return;
            }
            await _next(context);
        }

        private ClaimsPrincipal GetClaimPrincipal(string jwtToken)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSection.SecretKey));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true,
                IssuerSigningKey = securityKey
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            return tokenHandler.ValidateToken(jwtToken, tokenValidationParameters, out _);
        }

    }
}
