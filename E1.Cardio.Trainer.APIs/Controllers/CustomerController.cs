﻿using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace E1.Cardio.Trainer.APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController(IPackageService service, ITrainingTrackingService trainingTrackingService) : ControllerBase
    {
        private readonly IPackageService _service = service;
        private readonly ITrainingTrackingService _trainingTrackingService = trainingTrackingService;

        [HttpGet("packages/{cid}")]
        [Authorize(Policy = "Customer")]
        public async Task<IActionResult> GetPackages(long cid, string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetOrderedPackages(cid, query, pageIndex, pageSize);

            return Ok(result);
        }

        [HttpGet("{cid}")]
        [Authorize(Policy = "Customer")]
        public async Task<IActionResult> GetTrainingTrackingByCustomerId(int cid, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _trainingTrackingService.GetTrainingTrackingsByCustomerId(cid, pageIndex, pageSize);

            return Ok(result);
        }
    }
}
