﻿using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using E1.Cardio.Trainer.Libs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace E1.Cardio.Trainer.APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleRequestController(IScheduleChangeRequestService service) : ControllerBase
    {
        private readonly IScheduleChangeRequestService _service = service;

        [HttpGet]
        [Authorize(Policy = "Trainer")]
        public async Task<IActionResult> GetScheduleRequests(int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetScheduleRequests(pageIndex, pageSize);

            return Ok(result);
        }

        [HttpGet("search/{id}")]
        [Authorize(Policy = "Trainer")]
        public async Task<IActionResult> GetScheduleRequestById(int id)
        {
            var result = await _service.GetScheduleRequestById(id);

            return Ok(result);
        }

        [HttpGet("{cid}")]
        [Authorize(Policy = "Customer")]
        public async Task<IActionResult> GetScheduleRequestsByCustomerId(int cid, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetScheduleRequestsByCustomerId(cid, pageIndex, pageSize);

            return Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = "Customer")]
        public async Task<IActionResult> CreateScheduleRequest(CreateChangeRequestModel model)
        {
            var result = await _service.CreateScheduleChangeRequest(model);
            return Ok(result);
        }

        [HttpPut]
        [Authorize(Policy = "Customer")]
        public async Task<IActionResult> UpdateScheduleRequest(long id, CreateChangeRequestModel model)
        {
            var result = await _service.UpdateScheduleChangeRequest(id, model);
            return Ok(result);
        }

        [HttpPatch("confirm/{id}")]
        [Authorize(Policy = "Trainer")]
        public async Task<IActionResult> ConfirmScheduleRequest(long id)
        {
            var result = await _service.ConfirmScheduleChangeRequest(id);
            return Ok(result);
        }

    }
}
