﻿using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using E1.Cardio.Trainer.Libs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace E1.Cardio.Trainer.APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrainerController(ITrainingTrackingService trainingTrackingService) : ControllerBase
    {
        private readonly ITrainingTrackingService _trainingTrackingService = trainingTrackingService;

        [HttpGet]
        [Authorize(Policy = "Trainer")]
        public async Task<IActionResult> GetTrainingTrackings(int pageIndex = 1, int pageSize = 10)
        {
            var result = await _trainingTrackingService.GetTrainingTrackings(pageIndex, pageSize);

            return Ok(result);
        }

        [HttpGet("search/{id}")]
        [Authorize(Policy = "Trainer")]
        public async Task<IActionResult> GetTrainingTrackingById(int id)
        {
            var result = await _trainingTrackingService.GetTrainingTrackingById(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize(Policy = "Trainer")]
        public async Task<IActionResult> CreateTrainingTracking(CreateTrainingTrackingModel model)
        {
            var result = await _trainingTrackingService.CreateTrainingTracking(model);
            return Ok(result);
        }

        [HttpPut]
        [Authorize(Policy = "Trainer")]
        public async Task<IActionResult> UpdateTrainingTracking(long id, CreateTrainingTrackingModel model)
        {
            var result = await _trainingTrackingService.UpdateTrainingTracking(id, model);
            return Ok(result);
        }

    }
}
