﻿using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using E1.Cardio.Trainer.Libs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace E1.Cardio.Trainer.APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackageController(IPackageService service) : ControllerBase
    {
        private readonly IPackageService _service = service;

        [HttpGet]
        public async Task<IActionResult> GetPackages(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetPackages(query, pageIndex, pageSize);

            return Ok(result);
        }
        [HttpGet("demo")]
        public async Task<IActionResult> GetDemoPackages(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetDemoPackages(query, pageIndex, pageSize);

            return Ok(result);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPackage(long id)
        {
            var result = await _service.GetPackage(id);

            return Ok(result);
        }
        [Authorize(Policy = "Trainer")]
        [HttpPost("new_package")]
        public async Task<IActionResult> CreatePackage(long orderId)
        {
            var result = await _service.CreatePackage(orderId);

            return Ok(result);
        }
        [Authorize(Policy = "Trainer")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePackage(long id, [FromBody] PackageDetailsModel model)
        {
            var result = await _service.UpdatePackage(id, model);

            return Ok(result);
        }
        [Authorize(Policy = "Trainer")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> RemovePackage(long id)
        {
            var result = await _service.RemovePackage(id);

            return Ok(result);
        }
    }
}
