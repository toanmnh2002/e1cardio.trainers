﻿using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using E1.Cardio.Trainer.Libs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace E1.Cardio.Trainer.APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController(IAccountService service) : ControllerBase
    {
        private readonly IAccountService _service = service;

        [HttpPost("/login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var result = await _service.Login(model.Phone, model.Password);

            return Ok(result);
        }
        [HttpPost("/new_trainer")]
        public async Task<IActionResult> CreateTrainer([FromBody] TrainerModel model)
        {
            var result = await _service.CreateTrainer(model);

            return Ok(result);
        }
        [HttpPost("/new_customer")]
        public async Task<IActionResult> CreateCustomer([FromBody] CustomerModel model)
        {
            var result = await _service.CreateCustomer(model);

            return Ok(result);
        }

        [HttpPut("update")]
        public async Task<IActionResult> UpdateInfo([FromBody] dynamic model)
        {
            var result = await _service.UpdateInfo(model);

            return Ok(result);
        }

        [HttpGet("/info")]
        public async Task<IActionResult> GetInfo()
        {
            var result = await _service.GetInfo();

            return Ok(result);
        }
    }
}
