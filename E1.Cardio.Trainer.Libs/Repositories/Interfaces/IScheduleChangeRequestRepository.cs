﻿using E1.Cardio.BusinessObjects.Entities;

namespace E1.Cardio.Trainer.Libs.Repositories.Interfaces
{
    public interface IScheduleChangeRequestRepository
    {
        Task CreateScheduleChangeRequest(ScheduleRequest schedule);
        Task<List<ScheduleRequest>> GetScheduleChangeRequest(int pageIndex = 1, int pageSize = 10);
        Task<List<ScheduleRequest>> GetScheduleChangeRequestByCId(long cid, int pageIndex = 1, int pageSize = 10);
        Task<ScheduleRequest> GetScheduleChangeRequestById(long id);
        void RemoveScheduleChangeRequest(long id);
        void UpdateScheduleChangeRequest(ScheduleRequest schedule);
    }
}