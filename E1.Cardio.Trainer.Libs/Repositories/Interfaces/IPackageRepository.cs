﻿using E1.Cardio.BusinessObjects.Entities;

namespace E1.Cardio.Trainer.Libs.Repositories.Interfaces
{
    public interface IPackageRepository
    {
        Task CreatePackage(Package package);
        void UpdatePackage(Package package);
        Task<List<Package>> GetPackages(string? query, int pageIndex = 1, int pageSize = 10);
        Task<Package> GetPackageById(long id);
        void RemovePackage(long id);
        Task<DemoPackage> GetDemoPackage(long id);
        Task<List<DemoPackage>> GetDemoPackageList(string? query, int pageIndex = 1, int pageSize = 10);
        Task<List<Package>> GetOrderedPackages(long customerId, string? query, int pageIndex = 1, int pageSize = 10);
    }
}
