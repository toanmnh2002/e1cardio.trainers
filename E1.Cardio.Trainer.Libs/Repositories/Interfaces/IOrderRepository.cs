﻿using E1.Cardio.BusinessObjects.Entities;

namespace E1.Cardio.Trainer.Libs.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        Task<SelledPackage> GetSelledPackageById(long id);
        Task<List<SelledPackage>> GetSelledPackagesByTrainer(long id);
        Task CreateOrder(OrderedPackage order);
    }
}
