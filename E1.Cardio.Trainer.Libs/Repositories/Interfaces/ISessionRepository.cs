﻿using E1.Cardio.BusinessObjects.Entities;

namespace E1.Cardio.Trainer.Libs.Repositories.Interfaces
{
    public interface ISessionRepository
    {
        Task<Session?> GetSessionById(long id);
    }
}