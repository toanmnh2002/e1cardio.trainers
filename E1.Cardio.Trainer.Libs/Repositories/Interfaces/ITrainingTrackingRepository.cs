﻿using E1.Cardio.BusinessObjects.Entities;

namespace E1.Cardio.Trainer.Libs.Repositories.Interfaces
{
    public interface ITrainingTrackingRepository
    {
        Task CreateTrainingTracking(TrainingTracking trainingTracking);
        Task<List<TrainingTracking>> GetTrainingTracking(int pageIndex = 1, int pageSize = 10);
        Task<List<TrainingTracking>> GetTrainingTrackingByCId(long cid, int pageIndex = 1, int pageSize = 10);
        Task<TrainingTracking> GetTrainingTrackingById(long id);
        void RemoveTrainingTracking(long id);
        void UpdateTrainingTracking(TrainingTracking trainingTracking);
    }
}