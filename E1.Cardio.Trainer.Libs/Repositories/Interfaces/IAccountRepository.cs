﻿using E1.Cardio.BusinessObjects.Entities;

namespace E1.Cardio.Trainer.Libs.Repositories.Interfaces
{
    public interface IAccountRepository
    {
        Task<Account?> IsExist(string userName, string password);
        Task CreateTrainer(BusinessObjects.Entities.Trainer trainer);
        Task CreateCustomer(Customer customer);
        Task<Account?> GetAccountById(long id);
        void UpdateAccount(Account account);
    }
}
