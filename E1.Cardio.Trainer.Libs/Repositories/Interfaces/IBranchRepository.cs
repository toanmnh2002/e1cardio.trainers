﻿using E1.Cardio.BusinessObjects.Entities;

namespace E1.Cardio.Trainer.Libs.Repositories.Interfaces
{
    public interface IBranchRepository
    {
        Task<Branch?> GetBranchByUserId(long id);
    }
}