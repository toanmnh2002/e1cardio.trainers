﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace E1.Cardio.Trainer.Libs.Repositories
{
    public class SessionRepository(AppDbContext context) : ISessionRepository
    {
        private readonly AppDbContext _context = context;

        public async Task<Session?> GetSessionById(long id) => await _context.Sessions.FirstOrDefaultAsync(x => x.Id == id);
    }
}

