﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace E1.Cardio.Trainer.Libs.Repositories
{
    public class AccountRepository(AppDbContext context) : IAccountRepository
    {
        private readonly AppDbContext _context = context;

        public async Task CreateCustomer(Customer customer)
        {
            customer.CreatedDate = DateTime.UtcNow;
            customer.IsDeleted = false;
            await _context.Customers.AddAsync(customer);
        }

        public async Task CreateTrainer(BusinessObjects.Entities.Trainer trainer)
        {
            trainer.CreatedDate = DateTime.UtcNow;
            trainer.IsDeleted = false;
            await _context.Trainers.AddAsync(trainer);
        }

        public async Task<Account?> GetAccountById(long id)
        {
            return await _context.Accounts.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Account?> IsExist(string phone, string password)
        {
            return await _context.Accounts
                .FirstOrDefaultAsync(x =>
                    !x.IsDeleted
                    && x.Phone == phone
                    && x.Password == password);
        }

        public void UpdateAccount(Account account)
        {
            _context.Accounts.Update(account);
        }
    }
}
