﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace E1.Cardio.Trainer.Libs.Repositories
{
    public class TrainingTrackingRepository(AppDbContext context) : ITrainingTrackingRepository
    {
        private readonly AppDbContext _context = context;
        public async Task CreateTrainingTracking(TrainingTracking trainingTracking)
        {
            trainingTracking.CreatedDate = DateTime.UtcNow;
            trainingTracking.IsDeleted = false;
            await _context.TrainingTrackings.AddAsync(trainingTracking);
        }

        public async Task<List<TrainingTracking>> GetTrainingTracking(int pageIndex = 1, int pageSize = 10)
            => await _context.TrainingTrackings
                       .Skip(pageIndex)
                       .Take(pageSize * pageIndex)
                       .ToListAsync();

        public async Task<TrainingTracking> GetTrainingTrackingById(long id)
            => await _context.TrainingTrackings
                       .Include(x => x.Customer)
                       .Include(x => x.Trainer)
                       .Include(x => x.Session)
                       .FirstOrDefaultAsync(x => x.Id == id)!;

        public async Task<List<TrainingTracking>> GetTrainingTrackingByCId(long cid, int pageIndex = 1, int pageSize = 10)
            => await _context.TrainingTrackings
                       .Include(x => x.Customer)
                       .Include(x => x.Trainer)
                       .Include(x => x.Session)
                       .Skip(pageIndex)
                       .Take(pageSize * pageIndex)
                       .Where(x => x.CustomerId == cid)
                       .ToListAsync();

        public void RemoveTrainingTracking(long id)
        {
            var trainingTracking = _context.TrainingTrackings
                .FirstOrDefault(x => x.Id == id && !x.IsDeleted);
            if (trainingTracking != null)
                trainingTracking.IsDeleted = true;
        }
        public void UpdateTrainingTracking(TrainingTracking trainingTracking)
        {
            _context.TrainingTrackings.Update(trainingTracking);
        }
    }
}
