﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace E1.Cardio.Trainer.Libs.Repositories
{
    public class PackageRepository(AppDbContext context) : IPackageRepository
    {
        private readonly AppDbContext _context = context;

        public async Task CreatePackage(Package package)
        {
            package.CreatedDate = DateTime.UtcNow;
            package.IsDeleted = false;
            await _context.Packages.AddAsync(package);
        }

        public async Task<DemoPackage> GetDemoPackage(long id)
            => await _context.DemoPackages.Include(x => x.Branch).FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted);

        public async Task<List<DemoPackage>> GetDemoPackageList(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var demoPackages = _context.SelledPackages.GroupBy(x => x.DemoPackageId).OrderByDescending(gr => gr.Count());
            var demoPackageIds = demoPackages.Select(x => x.Key).ToList();
            if (demoPackageIds.Count > 0)
            {
                if (!string.IsNullOrEmpty(query))
                    return await _context.DemoPackages
                            .Where(x => x.PackageName.Contains(query)
                                || x.Type.Contains(query)
                                && !x.IsDeleted
                                && demoPackageIds.Contains(x.Id))
                            .Skip((pageIndex - 1) * pageSize)
                            .Take(pageSize)
                            .AsNoTracking()
                            .ToListAsync();

                return await _context.DemoPackages
                        .Where(x => !x.IsDeleted
                            && demoPackageIds.Contains(x.Id))
                        .Skip((pageIndex - 1) * pageSize)
                        .Take(pageSize)
                        .AsNoTracking()
                        .ToListAsync();
            }

            return await _context.DemoPackages
                        .Where(x => !x.IsDeleted)
                        .Skip((pageIndex - 1) * pageSize)
                        .Take(pageSize)
                        .AsNoTracking()
                        .ToListAsync();
        }

        public async Task<List<Package>> GetOrderedPackages(long customerId, string? query, int pageIndex = 1, int pageSize = 10)
        {
            var packageIds = await _context.OrderedPackages
                .Where(x => x.CustomerId == customerId)
                .Select(x => x.PackageId)
                .ToListAsync();
            if (!string.IsNullOrEmpty(query))
                return await _context.Packages
                        .Where(x => packageIds.Contains(x.Id)
                            && (x.PackageName.Contains(query) || x.Type.Contains(query))
                            && !x.IsDeleted)
                        .Include(x => x.Sessions)
                        .Skip((pageIndex - 1) * pageSize)
                        .Take(pageSize)
                        .AsNoTracking()
                        .ToListAsync();

            return await _context.Packages
                    .Where(x => !x.IsDeleted && packageIds.Contains(x.Id))
                    .Include(x => x.Sessions)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .ToListAsync();
        }

        public async Task<Package> GetPackageById(long id)
        {
            return await _context.Packages
                    .Include(x => x.Sessions)
                    .ThenInclude(x => x.SessionItems)
                    .Include(x => x.Sessions)
                    .ThenInclude(x => x.TrainedSessions)
                    .ThenInclude(x => x.Trainer)
                    .Include(x => x.TrainedPackages)
                    .ThenInclude(x => x.Trainer)
                    .Include(x => x.FollowedPackages)
                    .ThenInclude(x => x.Staff)
                    .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Package>> GetPackages(string? query, int pageIndex = 1, int pageSize = 10)
        {
            if (!string.IsNullOrEmpty(query))
                return await _context.Packages
                        .Where(x => x.PackageName.Contains(query) || x.Type.Contains(query) && !x.IsDeleted)
                        .Include(x => x.Sessions)
                        .Skip((pageIndex - 1) * pageSize)
                        .Take(pageSize)
                        .AsNoTracking()
                        .ToListAsync();

            return await _context.Packages
                    .Where(x => !x.IsDeleted)
                    .Include(x => x.Sessions)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .ToListAsync();
        }

        public void RemovePackage(long id)
        {
            var package = _context.Packages
                .FirstOrDefault(x => x.Id == id && !x.IsDeleted);
            if (package != null)
                package.IsDeleted = true;
        }
        public void UpdatePackage(Package package)
        {
            _context.Packages.Update(package);
        }
    }
}
