﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace E1.Cardio.Trainer.Libs.Repositories
{
    public class ScheduleChangeRequestRepository(AppDbContext context) : IScheduleChangeRequestRepository
    {
        private readonly AppDbContext _context = context;

        public async Task CreateScheduleChangeRequest(ScheduleRequest schedule)
        {
            schedule.CreatedDate = DateTime.UtcNow;
            schedule.IsDeleted = false;
            await _context.ScheduleRequests.AddAsync(schedule);
        }

        public async Task<List<ScheduleRequest>> GetScheduleChangeRequest(int pageIndex = 1, int pageSize = 10)
            => await _context.ScheduleRequests
                       .Skip(pageIndex)
                       .Take(pageSize * pageIndex)
                       .ToListAsync();

        public async Task<ScheduleRequest> GetScheduleChangeRequestById(long id)
            => await _context.ScheduleRequests
                      .Include(x => x.Customer)
                       .Include(x => x.Trainer)
                       .Include(x => x.Session)
                       .FirstOrDefaultAsync(x => x.Id == id)!;

        public async Task<List<ScheduleRequest>> GetScheduleChangeRequestByCId(long cid, int pageIndex = 1, int pageSize = 10)
            => await _context.ScheduleRequests
                      .Include(x => x.Customer)
                       .Include(x => x.Trainer)
                       .Include(x => x.Session)
                       .Skip(pageIndex)
                       .Take(pageSize * pageIndex)
                       .Where(x => x.CustomerId == cid)
                       .ToListAsync();

        public void RemoveScheduleChangeRequest(long id)
        {
            var schedule = _context.ScheduleRequests
                .FirstOrDefault(x => x.Id == id && !x.IsDeleted);
            if (schedule != null)
                schedule.IsDeleted = true;
        }
        public void UpdateScheduleChangeRequest(ScheduleRequest schedule)
        {
            _context.ScheduleRequests.Update(schedule);
        }
    }
}
