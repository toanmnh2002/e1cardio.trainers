﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace E1.Cardio.Trainer.Libs.Repositories
{
    public class OrderRepository(AppDbContext appDbContext) : IOrderRepository
    {
        private readonly AppDbContext _appDbContext = appDbContext;

        public async Task CreateOrder(OrderedPackage order)
        {
            await _appDbContext.OrderedPackages.AddAsync(order);
        }

        public async Task<SelledPackage> GetSelledPackageById(long id)
            => await _appDbContext.SelledPackages
            .Include(x => x.DemoPackages)
            .FirstOrDefaultAsync(x => x.Id == id);

        public async Task<List<SelledPackage>> GetSelledPackagesByTrainer(long id)
            => await _appDbContext.SelledPackages
            .Include(x => x.DemoPackages)
            .Where(x => x.CreatePackageTrainer == id)
            .ToListAsync();
    }
}
