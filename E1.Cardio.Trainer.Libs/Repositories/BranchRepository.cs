﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace E1.Cardio.Trainer.Libs.Repositories
{
    public class BranchRepository(AppDbContext context) : IBranchRepository
    {
        private readonly AppDbContext _context = context;
        public async Task<Branch?> GetBranchByUserId(long id) => await _context!.Branches!.FirstOrDefaultAsync(x => x.Id == id)!;
    }
}
