﻿using AutoMapper;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.BusinessObjects.Models;
using E1.Cardio.Trainer.Libs.Models;
using Newtonsoft.Json;

namespace E1.Cardio.Trainer.Libs
{
    public class MapperConfigs : Profile
    {
        public MapperConfigs()
        {
            CreateMap<CustomerModel, Customer>().ReverseMap();
            CreateMap<UpdateCustomerModel, Customer>().ReverseMap();
            CreateMap<TrainerModel, BusinessObjects.Entities.Trainer>().ReverseMap();

            CreateMap<DemoPackage, Package>().ReverseMap();
            CreateMap<PackageModel, Package>()
                .ForMember(des => des.DefaultSchedule,
                           act => act.MapFrom(src => JsonConvert.SerializeObject(src.Schedules)))
                .ReverseMap()
                .ForMember(des => des.Schedules,
                           act => act.MapFrom(src => JsonConvert.DeserializeObject<List<ScheduleModel>>(src.DefaultSchedule)));
            CreateMap<PackageDetailsModel, Package>()
                .ForMember(des => des.DefaultSchedule,
                           act => act.MapFrom(src => JsonConvert.SerializeObject(src.Schedules)))
                .ReverseMap()
                .ForMember(des => des.Schedules,
                           act => act.MapFrom(src => JsonConvert.DeserializeObject<List<ScheduleModel>>(src.DefaultSchedule))); ;
            CreateMap<SessionModel, Session>().ReverseMap();
            CreateMap<SessionItemModel, SessionItem>().ReverseMap();

            CreateMap<UpdatePackageModel, Package>()
                .ForMember(des => des.DefaultSchedule,
                           act => act.MapFrom(src => JsonConvert.SerializeObject(src.Schedules)))
                .ReverseMap()
                .ForMember(des => des.Schedules,
                           act => act.MapFrom(src => JsonConvert.DeserializeObject<List<ScheduleModel>>(src.DefaultSchedule))); ;
            CreateMap<UpdateSessionModel, Session>();
            CreateMap<UpdateSessionItemModel, SessionItem>();
            CreateMap<DemoPackageModel, DemoPackage>().ReverseMap();
            CreateMap<BranchModel, Branch>().ReverseMap();
        }
    }
}
