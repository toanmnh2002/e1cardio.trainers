﻿namespace E1.Cardio.Trainer.Libs.Models
{
    public class ScheduleChangeRequestModel
    {
        public DateTime ChangeTimeTo { get; set; }
        public long CustomerId { get; set; }
        public CustomerAddModel? Customer { get; set; }
        public long? TrainerId { get; set; }
        public TrainerAddModel? Trainer { get; set; }
        public long SessionId { get; set; }
        public long BranchId { get; set; }
        public BranchModel? Branch { get; set; }
        public bool IsConfirm { get; set; }
    }

    public class CreateChangeRequestModel
    {
        public DateTime ChangeTimeTo { get; set; }
        public long CustomerId { get; set; }
        public long? TrainerId { get; set; }
        public long SessionId { get; set; }
        public long BranchId { get; set; }
    }

    public class CustomerAddModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Email { get; set; }
        public string? Address { get; set; }
    }

    public class TrainerAddModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Email { get; set; }
        public string? Address { get; set; }
    }

}
