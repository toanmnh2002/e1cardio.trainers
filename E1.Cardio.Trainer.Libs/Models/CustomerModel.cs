﻿namespace E1.Cardio.Trainer.Libs.Models
{
    public class CustomerModel
    {
        public string Phone { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Email { get; set; }
        public DateTime DOB { get; set; }
        public string? Address { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public double? MuscleRatio { get; set; }
        public double? FatRatio { get; set; }
        public double? VisceralFatLevels { get; set; }
    }
    public class UpdateCustomerModel
    {
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Email { get; set; }
        public DateTime DOB { get; set; }
        public string? Address { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public double? MuscleRatio { get; set; }
        public double? FatRatio { get; set; }
        public double? VisceralFatLevels { get; set; }
    }
}
