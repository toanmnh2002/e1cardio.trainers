﻿namespace E1.Cardio.Trainer.Libs.Models.AppModels
{
    public class Jwt
    {
        public string SecretKey { get; set; }
        public int ExpiresInDays { get; set; }

    }
}
