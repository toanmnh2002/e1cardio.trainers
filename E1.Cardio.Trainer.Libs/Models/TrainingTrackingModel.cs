﻿namespace E1.Cardio.Trainer.Libs.Models
{
    public class TrainingTrackingModel
    {
        public DateTime TrackingDate { get; set; }
        public long? CustomerId { get; set; }
        public CustomerAddModel? Customer { get; set; }
        public long? TrainerId { get; set; }
        public TrainerAddModel? Trainer { get; set; }
        public long? SessionId { get; set; }
        public long? BranchId { get; set; }
        public BranchModel? Branch { get; set; }
        public double EnergyPoint { get; set; }
    }

    public class CreateTrainingTrackingModel
    {
        public DateTime ChangeTimeTo { get; set; }
        public long CustomerId { get; set; }
        public long? TrainerId { get; set; }
        public long SessionId { get; set; }
        public long BranchId { get; set; }
    }
}
