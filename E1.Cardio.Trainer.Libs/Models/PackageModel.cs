﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.BusinessObjects.Models;

namespace E1.Cardio.Trainer.Libs.Models
{
    public class PackageModel
    {
        public string PackageName { get; set; }
        public double NumberOfDays { get; set; }
        public double NumberOfSessions { get; set; }
        public decimal PackagePrice { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public PackageStatus Status { get; set; }
        public List<ScheduleModel>? Schedules { get; set; }
    }
    public class PackageDetailsModel
    {
        public long Id { get; set; }
        public string PackageName { get; set; }
        public double NumberOfDays { get; set; }
        public double NumberOfSessions { get; set; }
        public decimal PackagePrice { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public PackageStatus Status { get; set; }
        public ICollection<SessionModel>? Sessions { get; set; }
        public List<ScheduleModel>? Schedules { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
    public class SessionModel
    {
        public long? Id { get; set; }
        public string Title { get; set; }
        public string? Outcome { get; set; }
        public string? Descriptions { get; set; }
        public double EnergyPoint { get; set; }
        public ICollection<SessionItemModel>? SessionItems { get; set; }
        public BranchModel Branch { get; set; }
    }
    public class SessionItemModel
    {
        public long? Id { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public string? ImageUri { get; set; }
    }
    public class UpdatePackageModel
    {
        public string PackageName { get; set; }
        public double NumberOfDays { get; set; }
        public double NumberOfSessions { get; set; }
        public decimal PackagePrice { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public PackageStatus Status { get; set; }
        public ICollection<UpdateSessionModel>? Sessions { get; set; }
        public List<ScheduleModel>? Schedules { get; set; }
        public long? CreatedById { get; set; }
        public TrainerModel? CreateBy { get; set; }
        public long? TrainById { get; set; }
        public TrainerModel? TrainBy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
    public class UpdateSessionModel
    {
        public string Title { get; set; }
        public string? Outcome { get; set; }
        public string? Descriptions { get; set; }
        public double EnergyPoint { get; set; }
        public ICollection<UpdateSessionItemModel>? SessionItems { get; set; }
    }
    public class UpdateSessionItemModel
    {
        public string Title { get; set; }
        public string? Description { get; set; }
        public string? ImageUri { get; set; }
    }
    public class DemoPackageModel
    {
        public string PackageName { get; set; }
        public string Descriptions { get; set; }
        public double NumberOfDays { get; set; }
        public double NumberOfSessions { get; set; }
        public decimal PackagePrice { get; set; }
        public string Type { get; set; }
        public BranchModel? Branch { get; set; }
    }
    public class BranchModel
    {
        public string BranchName { get; set; }
        public string Location { get; set; }
        public string Phone { get; set; }
    }
}
