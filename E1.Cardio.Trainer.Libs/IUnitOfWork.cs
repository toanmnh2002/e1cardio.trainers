﻿using E1.Cardio.Trainer.Libs.Repositories.Interfaces;

namespace E1.Cardio.Trainer.Libs
{
    public interface IUnitOfWork
    {
        public IAccountRepository AccountRepository { get; }
        public IPackageRepository PackageRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IScheduleChangeRequestRepository ScheduleChangeRequestRepository { get; }
        public IBranchRepository BranchRepository
        { get; }
        public ISessionRepository SessionRepository { get; }
        public ITrainingTrackingRepository TrainingTrackingRepository { get; }

        Task<int> SaveChangeAsync();
    }
}
