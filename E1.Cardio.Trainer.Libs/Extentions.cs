﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.Trainer.Libs.AppServices;
using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using E1.Cardio.Trainer.Libs.Models.AppModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace E1.Cardio.Trainer.Libs
{
    public static class Extentions
    {
        public static IServiceCollection LibsExtention
            (this IServiceCollection services,
            IConfiguration config)
        {
            services.AddDbContext<AppDbContext>(option => option
                .UseNpgsql(config
                    .GetConnectionString("E1Cardio-Local")));

            services.AddSingleton(config
                .GetSection("Jwt")
                .Get<Jwt>());

            services.AddAutoMapper
                (typeof(MapperConfigs).Assembly);

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IPackageService, PackageService>();

            return services;
        }
    }
}
