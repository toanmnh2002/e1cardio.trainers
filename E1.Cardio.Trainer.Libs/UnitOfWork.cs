﻿using E1.Cardio.BusinessObjects;
using E1.Cardio.Trainer.Libs.Repositories;
using E1.Cardio.Trainer.Libs.Repositories.Interfaces;

namespace E1.Cardio.Trainer.Libs
{
    public class UnitOfWork(AppDbContext context) : IUnitOfWork
    {
        private readonly AppDbContext _context = context;

        public IAccountRepository AccountRepository
            => new AccountRepository(_context);

        public IPackageRepository PackageRepository
            => new PackageRepository(_context);

        public IOrderRepository OrderRepository
            => new OrderRepository(_context);
        public IScheduleChangeRequestRepository ScheduleChangeRequestRepository
            => new ScheduleChangeRequestRepository(_context);

        public IBranchRepository BranchRepository
            => new BranchRepository(_context);

        public ISessionRepository SessionRepository
           => new SessionRepository(_context);

        public ITrainingTrackingRepository TrainingTrackingRepository
           => new TrainingTrackingRepository(_context);
        public async Task<int> SaveChangeAsync()
            => await _context.SaveChangesAsync();
    }
}
