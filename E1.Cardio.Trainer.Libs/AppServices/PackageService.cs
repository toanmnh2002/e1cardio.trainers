﻿using AutoMapper;
using E1.Cardio.BusinessObjects;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using E1.Cardio.Trainer.Libs.Models;
using E1.Cardio.Trainer.Libs.Models.AppModels;

namespace E1.Cardio.Trainer.Libs.AppServices
{
    public class PackageService(IUnitOfWork unitOfWork, IMapper mapper) : IPackageService
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        private readonly IMapper _mapper = mapper;

        public async Task<ResponseModel<PackageDetailsModel>> CreatePackage(long orderId)
        {
            var selledPackageObj = await _unitOfWork.OrderRepository.GetSelledPackageById(orderId);
            var demoPackageObj = selledPackageObj?.DemoPackages;
            if (selledPackageObj == null || demoPackageObj == null)
                return new ResponseModel<PackageDetailsModel>
                {
                    Errors = "Not found order."
                };
            if (await _unitOfWork.AccountRepository.GetAccountById(selledPackageObj.CustomerId) is not Customer customer)
                return new ResponseModel<PackageDetailsModel>
                {
                    Errors = "Not found customer."
                };

            var packageObj = new Package();
            _mapper.Map(demoPackageObj, packageObj);

            packageObj.Status = PackageStatus.InProcess;
            packageObj.Price = demoPackageObj.PackagePrice;
            packageObj.SelledPackageId = orderId;
            packageObj.DefaultSchedule = selledPackageObj.Schedules;
            if (selledPackageObj.CreatePackageTrainer != null)
                packageObj.CreatedPackages = new List<CreatedPackage>
                {
                    new() 
                    {
                        TrainerId = selledPackageObj.TrainPackageTrainer.Value
                    }
                };
            if (selledPackageObj.TrainPackageTrainer != null)
                packageObj.TrainedPackages = new List<TrainedPackage>
                {
                    new()
                    { 
                        TrainerId = selledPackageObj.TrainPackageTrainer.Value
                    }
                };
            packageObj.FollowedPackages = new List<FollowedPackage>
            {
                new() 
                {
                    StaffId = selledPackageObj.StaffId
                }
            };

            await _unitOfWork.PackageRepository.CreatePackage(packageObj);

            var orderedPackage = new OrderedPackage
            {
                TotalPrice = packageObj.PackagePrice,
                OrderedDate = DateTime.UtcNow,
                Package = packageObj,
                StartDate = packageObj?.StartDate,
                EndDate = packageObj?.EndDate,
                Customer = customer
            };
            await _unitOfWork.OrderRepository.CreateOrder(orderedPackage);

            var rs = await _unitOfWork.SaveChangeAsync();
            if (rs <= 0)
                return new ResponseModel<PackageDetailsModel>
                {
                    Errors = "Create fail."
                };
            var dataResponse = _mapper.Map<PackageDetailsModel>(packageObj);

            return new ResponseModel<PackageDetailsModel>
            {
                Data = dataResponse
            };
        }

        public async Task<ResponseModel<List<DemoPackageModel>>> GetDemoPackages(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var demoPackages = await _unitOfWork.PackageRepository.GetDemoPackageList(query, pageIndex, pageSize);
            var dataResponse = _mapper.Map<List<DemoPackageModel>>(demoPackages);

            return new ResponseModel<List<DemoPackageModel>>
            {
                Data = dataResponse
            };
        }

        public async Task<ResponseModel<List<PackageDetailsModel>>> GetOrderedPackages(long customerId, string? query, int pageIndex = 1, int pageSize = 10)
        {
            var orderedPackages = await _unitOfWork.PackageRepository.GetOrderedPackages(customerId, query, pageIndex, pageSize);
            var dataResponse = _mapper.Map<List<PackageDetailsModel>>(orderedPackages);

            return new ResponseModel<List<PackageDetailsModel>>
            {
                Data = dataResponse
            };
        }

        public async Task<ResponseModel<PackageDetailsModel>> GetPackage(long id)
        {
            var rs = await _unitOfWork.PackageRepository.GetPackageById(id);
            var dataResponse = _mapper.Map<PackageDetailsModel>(rs);

            return new ResponseModel<PackageDetailsModel>
            {
                Data = dataResponse
            };
        }

        public async Task<ResponseModel<List<PackageDetailsModel>>> GetPackages(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var rs = await _unitOfWork.PackageRepository.GetPackages(query, pageIndex, pageSize);
            var dataResponse = _mapper.Map<List<PackageDetailsModel>>(rs);

            return new ResponseModel<List<PackageDetailsModel>>
            {
                Data = dataResponse
            };
        }

        public async Task<ResponseModel<string>> RemovePackage(long id)
        {
            _unitOfWork.PackageRepository.RemovePackage(id);
            var rs = await _unitOfWork.SaveChangeAsync();
            if (rs <= 0)
                return new ResponseModel<string>
                {
                    Errors = "Remove fail."
                };

            return new ResponseModel<string>
            {
                Data = "Removed"
            };
        }

        public async Task<ResponseModel<PackageDetailsModel>> UpdatePackage(long id, PackageDetailsModel model)
        {
            var packageObj = await _unitOfWork.PackageRepository.GetPackageById(id);
            if (packageObj == null)
                return new ResponseModel<PackageDetailsModel>
                {
                    Errors = "Not found package"
                };

            _mapper.Map(model, packageObj);
            _unitOfWork.PackageRepository.UpdatePackage(packageObj);
            await _unitOfWork.SaveChangeAsync();

            return new ResponseModel<PackageDetailsModel>
            {
                Data = _mapper.Map<PackageDetailsModel>(packageObj),
            };
        }
    }
}
