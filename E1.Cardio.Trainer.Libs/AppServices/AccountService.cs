﻿using AutoMapper;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using E1.Cardio.Trainer.Libs.Models;
using E1.Cardio.Trainer.Libs.Models.AppModels;
using E1.Cardio.Trainer.Libs.Utils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Dynamic;

namespace E1.Cardio.Trainer.Libs.AppServices
{
    public class AccountService(IUnitOfWork unitOfWork,
        IMapper mapper,
        Jwt jwt,
        IHttpContextAccessor httpContext) : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        private readonly IMapper _mapper = mapper;
        private readonly Jwt _jwt = jwt;
        private readonly IHttpContextAccessor _httpContext = httpContext;

        public async Task<ResponseModel<CustomerModel>> CreateCustomer(CustomerModel model)
        {
            var customerObj = _mapper.Map<Customer>(model);
            await _unitOfWork.AccountRepository.CreateCustomer(customerObj);
            var rs = await _unitOfWork.SaveChangeAsync();
            if (rs <= 0)
            {
                return new ResponseModel<CustomerModel>
                {
                    Errors = "Create fail."
                };
            }

            return new ResponseModel<CustomerModel>
            {
                Data = model
            };
        }

        public async Task<ResponseModel<TrainerModel>> CreateTrainer(TrainerModel model)
        {
            var trainerObj = _mapper.Map<BusinessObjects.Entities.Trainer>(model);
            await _unitOfWork.AccountRepository.CreateTrainer(trainerObj);
            var rs = await _unitOfWork.SaveChangeAsync();
            if (rs <= 0)
            {
                return new ResponseModel<TrainerModel>
                {
                    Errors = "Create fail."
                };
            }

            return new ResponseModel<TrainerModel>
            {
                Data = model
            };
        }

        public async Task<ResponseModel<dynamic>> GetInfo()
        {
            var currentId = _httpContext.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            if (currentId == null)
                return new ResponseModel<dynamic>
                {
                    Errors = "Not found"
                };
            long.TryParse(currentId, out var id);
            var account = await _unitOfWork.AccountRepository.GetAccountById(id);
            if (account == null)
                return new ResponseModel<dynamic>
                {
                    Errors = "Not found."
                };

            return new ResponseModel<dynamic>
            {
                Data = account is Customer ? _mapper.Map<CustomerModel>(account)
                : account is E1.Cardio.BusinessObjects.Entities.Trainer ? _mapper.Map<TrainerModel>(account)
                    : null
            };
        }

        public async Task<ResponseModel<string>> Login(string username, string password)
        {
            var account = await _unitOfWork.AccountRepository.IsExist(username, password);
            if (account == null)
                return new ResponseModel<string> { Errors = "Login fail." };

            var token = account.GenerateToken(_jwt);

            return new ResponseModel<string> { Data = token };
        }

        public async Task<ResponseModel<dynamic>> UpdateInfo(object model)
        {
            var currentId = _httpContext.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            if (currentId == null)
                return new ResponseModel<dynamic>
                {
                    Errors = "Not found"
                };
            long.TryParse(currentId, out var id);
            var account = await _unitOfWork.AccountRepository.GetAccountById(id);
            if (account == null)
                return new ResponseModel<dynamic>
                {
                    Errors = "Not found."
                };

            var stringModel = model.ToString();
            if (account is BusinessObjects.Entities.Trainer)
            {
                var mapped = JsonConvert.DeserializeObject<TrainerModel>(stringModel);
                
            }
            else if (account is Customer)
            {
                var mapped = JsonConvert.DeserializeObject<CustomerModel>(stringModel);
                _mapper.Map(mapped, account);
            }
           
            _unitOfWork.AccountRepository.UpdateAccount(account);
            var rs = await _unitOfWork.SaveChangeAsync();
            if (rs <= 0)
            {
                return new ResponseModel<dynamic>
                {
                    Errors = "Update fail."
                };
            }
            return new ResponseModel<dynamic>
            {
                Data = account
            };
        }
    }
}
