﻿using AutoMapper;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using E1.Cardio.Trainer.Libs.Models;
using E1.Cardio.Trainer.Libs.Models.AppModels;

namespace E1.Cardio.Trainer.Libs.AppServices
{
    public class TrainingTrackingService : ITrainingTrackingService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public TrainingTrackingService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<ResponseModel<TrainingTrackingModel>> CreateTrainingTracking(CreateTrainingTrackingModel model)
        {
            var trainingTracking = _mapper.Map<TrainingTracking>(model);

            await _unitOfWork.TrainingTrackingRepository.CreateTrainingTracking(trainingTracking);
            var isSuccess = await _unitOfWork.SaveChangeAsync();

            if (isSuccess <= 0)
            {
                return new ResponseModel<TrainingTrackingModel>
                {
                    Errors = "Create fail."
                };
            }

            var responseModel = _mapper.Map<TrainingTrackingModel>(trainingTracking);
            return new ResponseModel<TrainingTrackingModel>
            {
                Data = responseModel
            };
        }

        public async Task<ResponseModel<TrainingTrackingModel>> GetTrainingTrackingById(long id)
        {
            var trainingTracking = await _unitOfWork.TrainingTrackingRepository.GetTrainingTrackingById(id);
            if (trainingTracking == null)
            {
                return new ResponseModel<TrainingTrackingModel>
                {
                    Errors = "Not found TrainingTracking"
                };
            }

            var responseModel = _mapper.Map<TrainingTrackingModel>(trainingTracking);
            return new ResponseModel<TrainingTrackingModel>
            {
                Data = responseModel
            };
        }

        public async Task<ResponseModel<List<TrainingTrackingModel>>> GetTrainingTrackingsByCustomerId(long customerId, int pageIndex = 1, int pageSize = 10)
        {
            var trainingTrackings = await _unitOfWork.TrainingTrackingRepository.GetTrainingTrackingByCId(customerId, pageIndex, pageSize);
            var responseModel = _mapper.Map<List<TrainingTrackingModel>>(trainingTrackings);

            return new ResponseModel<List<TrainingTrackingModel>>
            {
                Data = responseModel
            };
        }

        public async Task<ResponseModel<List<TrainingTrackingModel>>> GetTrainingTrackings(int pageIndex = 1, int pageSize = 10)
        {
            var trainingTrackings = await _unitOfWork.TrainingTrackingRepository.GetTrainingTracking(pageIndex, pageSize);
            var responseModel = _mapper.Map<List<TrainingTrackingModel>>(trainingTrackings);

            return new ResponseModel<List<TrainingTrackingModel>>
            {
                Data = responseModel
            };
        }

        public async Task<ResponseModel<TrainingTrackingModel>> UpdateTrainingTracking(long id, CreateTrainingTrackingModel model)
        {
            var trainingTracking = await _unitOfWork.TrainingTrackingRepository.GetTrainingTrackingById(id);

            if (trainingTracking == null)
            {
                return new ResponseModel<TrainingTrackingModel>
                {
                    Errors = "Not found TrainingTracking"
                };
            }

            _mapper.Map(model, trainingTracking);
            _unitOfWork.TrainingTrackingRepository.UpdateTrainingTracking(trainingTracking);
            await _unitOfWork.SaveChangeAsync();

            var responseModel = _mapper.Map<TrainingTrackingModel>(trainingTracking);
            return new ResponseModel<TrainingTrackingModel>
            {
                Data = responseModel
            };
        }

        public async Task<ResponseModel<string>> DeleteTrainingTracking(long id)
        {
            _unitOfWork.TrainingTrackingRepository.RemoveTrainingTracking(id);
            var isSuccess = await _unitOfWork.SaveChangeAsync();

            if (isSuccess <= 0)
            {
                return new ResponseModel<string>
                {
                    Errors = "Delete fail."
                };
            }

            return new ResponseModel<string>
            {
                Data = "Deleted"
            };
        }
    }
}
