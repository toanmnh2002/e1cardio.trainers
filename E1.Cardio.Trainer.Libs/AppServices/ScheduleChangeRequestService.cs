﻿using AutoMapper;
using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.AppServices.Interfaces;
using E1.Cardio.Trainer.Libs.Models;
using E1.Cardio.Trainer.Libs.Models.AppModels;
using Microsoft.AspNetCore.Http;

namespace E1.Cardio.Trainer.Libs.AppServices
{
    public class ScheduleChangeRequestService(IUnitOfWork unitOfWork, IMapper mapper, IHttpContextAccessor httpContextAccessor) : IScheduleChangeRequestService
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        private readonly IMapper _mapper = mapper;
        private readonly IHttpContextAccessor _httpContextAccessor = httpContextAccessor;
        public async Task<ResponseModel<ScheduleChangeRequestModel>> CreateScheduleChangeRequest(CreateChangeRequestModel model)
        {
            var currentUser = _httpContextAccessor.HttpContext.User?.Claims?.FirstOrDefault(x => x.Type == "id")?.Value;

            if (currentUser is null or "")
            {
                return new ResponseModel<ScheduleChangeRequestModel>
                {
                    Errors = "Not login yet!"
                };
            }

            _ = long.TryParse(currentUser, out long id);


            var branch = _unitOfWork.BranchRepository.GetBranchByUserId(model.BranchId);
            if (branch is null)
            {
                return new ResponseModel<ScheduleChangeRequestModel>
                {
                    Errors = "Not found branch!"
                };
            }

            var session = _unitOfWork.SessionRepository.GetSessionById(model.SessionId);
            if (branch is null)
            {
                return new ResponseModel<ScheduleChangeRequestModel>
                {
                    Errors = "Not found session!"
                };
            }

            var scheduleChangeRequest = _mapper.Map<ScheduleRequest>(model);
            scheduleChangeRequest.CustomerId = id;

            await _unitOfWork.ScheduleChangeRequestRepository.CreateScheduleChangeRequest(scheduleChangeRequest);
            var isSuccess = await _unitOfWork.SaveChangeAsync();

            if (isSuccess <= 0)
                return new ResponseModel<ScheduleChangeRequestModel>
                {
                    Errors = "Create fail."
                };
            return new ResponseModel<ScheduleChangeRequestModel>
            {
                Data = _mapper.Map<ScheduleChangeRequestModel>(scheduleChangeRequest)
            };
        }

        public async Task<ResponseModel<ScheduleChangeRequestModel>> GetScheduleRequestById(long id)
        {
            var scheduleRequest = await _unitOfWork.ScheduleChangeRequestRepository.GetScheduleChangeRequestById(id);
            var dataResponse = _mapper.Map<ScheduleChangeRequestModel>(scheduleRequest);

            return new ResponseModel<ScheduleChangeRequestModel>
            {
                Data = dataResponse
            };
        }

        public async Task<ResponseModel<List<ScheduleChangeRequestModel>>> GetScheduleRequestsByCustomerId(long cid, int pageIndex = 1, int pageSize = 10)
        {
            var scheduleRequests = await _unitOfWork.ScheduleChangeRequestRepository.GetScheduleChangeRequestByCId(cid, pageIndex, pageSize);
            var dataResponse = _mapper.Map<List<ScheduleChangeRequestModel>>(scheduleRequests);

            return new ResponseModel<List<ScheduleChangeRequestModel>>
            {
                Data = dataResponse
            };
        }

        public async Task<ResponseModel<List<ScheduleChangeRequestModel>>> GetScheduleRequests(int pageIndex = 1, int pageSize = 10)
        {
            var scheduleRequests = await _unitOfWork.ScheduleChangeRequestRepository.GetScheduleChangeRequest(pageIndex, pageSize);
            var dataResponse = _mapper.Map<List<ScheduleChangeRequestModel>>(scheduleRequests);

            return new ResponseModel<List<ScheduleChangeRequestModel>>
            {
                Data = dataResponse
            };
        }

        public async Task<ResponseModel<ScheduleChangeRequestModel>> UpdateScheduleChangeRequest(long id, CreateChangeRequestModel model)
        {
            var scheduleRequest = await _unitOfWork.ScheduleChangeRequestRepository.GetScheduleChangeRequestById(id);
            if (scheduleRequest == null)
                return new ResponseModel<ScheduleChangeRequestModel>
                {
                    Errors = "Not found ScheduleChangeRequest"
                };

            _mapper.Map(model, scheduleRequest);
            _unitOfWork.ScheduleChangeRequestRepository.UpdateScheduleChangeRequest(scheduleRequest);
            await _unitOfWork.SaveChangeAsync();

            return new ResponseModel<ScheduleChangeRequestModel>
            {
                Data = _mapper.Map<ScheduleChangeRequestModel>(scheduleRequest),
            };
        }

        public async Task<ResponseModel<ScheduleChangeRequestModel>> ConfirmScheduleChangeRequest(long id)
        {
            var scheduleRequest = await _unitOfWork.ScheduleChangeRequestRepository.GetScheduleChangeRequestById(id);
            if (scheduleRequest == null)
                return new ResponseModel<ScheduleChangeRequestModel>
                {
                    Errors = "Not found ScheduleChangeRequest"
                };

            scheduleRequest.IsConfirm = true;
            _unitOfWork.ScheduleChangeRequestRepository.UpdateScheduleChangeRequest(scheduleRequest);
            await _unitOfWork.SaveChangeAsync();

            return new ResponseModel<ScheduleChangeRequestModel>
            {
                Data = _mapper.Map<ScheduleChangeRequestModel>(scheduleRequest),
            };
        }

        public async Task<ResponseModel<string>> RemoveScheduleChangeRequest(long id)
        {
            _unitOfWork.ScheduleChangeRequestRepository.RemoveScheduleChangeRequest(id);
            var rs = await _unitOfWork.SaveChangeAsync();
            if (rs <= 0)
                return new ResponseModel<string>
                {
                    Errors = "Remove fail."
                };

            return new ResponseModel<string>
            {
                Data = "Removed"
            };
        }
    }
}
