﻿using E1.Cardio.Trainer.Libs.Models;
using E1.Cardio.Trainer.Libs.Models.AppModels;

namespace E1.Cardio.Trainer.Libs.AppServices.Interfaces
{
    public interface IAccountService
    {
        Task<ResponseModel<string>> Login(string username, string password);
        Task<ResponseModel<CustomerModel>> CreateCustomer(CustomerModel model);
        Task<ResponseModel<TrainerModel>> CreateTrainer(TrainerModel model);
        Task<ResponseModel<dynamic>> UpdateInfo(dynamic model);
        Task<ResponseModel<dynamic>> GetInfo();
    }
}
