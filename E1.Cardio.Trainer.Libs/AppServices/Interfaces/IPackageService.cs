﻿using E1.Cardio.Trainer.Libs.Models;
using E1.Cardio.Trainer.Libs.Models.AppModels;

namespace E1.Cardio.Trainer.Libs.AppServices.Interfaces
{
    public interface IPackageService
    {
        Task<ResponseModel<PackageDetailsModel>> CreatePackage(long orderId);
        Task<ResponseModel<string>> RemovePackage(long id);
        Task<ResponseModel<PackageDetailsModel>> UpdatePackage(long id, PackageDetailsModel model);
        Task<ResponseModel<List<PackageDetailsModel>>> GetPackages(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<PackageDetailsModel>> GetPackage(long id);
        Task<ResponseModel<List<DemoPackageModel>>> GetDemoPackages(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<List<PackageDetailsModel>>> GetOrderedPackages(long customerId, string? query, int pageIndex = 1, int pageSize = 10);
    }
}
