﻿using E1.Cardio.Trainer.Libs.Models;
using E1.Cardio.Trainer.Libs.Models.AppModels;

namespace E1.Cardio.Trainer.Libs.AppServices.Interfaces
{
    public interface IScheduleChangeRequestService
    {
        Task<ResponseModel<ScheduleChangeRequestModel>> ConfirmScheduleChangeRequest(long id);
        Task<ResponseModel<ScheduleChangeRequestModel>> CreateScheduleChangeRequest(CreateChangeRequestModel model);
        Task<ResponseModel<ScheduleChangeRequestModel>> GetScheduleRequestById(long id);
        Task<ResponseModel<List<ScheduleChangeRequestModel>>> GetScheduleRequests(int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<List<ScheduleChangeRequestModel>>> GetScheduleRequestsByCustomerId(long cid, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<string>> RemoveScheduleChangeRequest(long id);
        Task<ResponseModel<ScheduleChangeRequestModel>> UpdateScheduleChangeRequest(long id, CreateChangeRequestModel model);
    }
}