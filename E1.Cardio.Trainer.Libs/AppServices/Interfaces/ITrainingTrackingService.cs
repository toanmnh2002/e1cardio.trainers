﻿using E1.Cardio.Trainer.Libs.Models;
using E1.Cardio.Trainer.Libs.Models.AppModels;

namespace E1.Cardio.Trainer.Libs.AppServices.Interfaces
{
    public interface ITrainingTrackingService
    {
        Task<ResponseModel<TrainingTrackingModel>> CreateTrainingTracking(CreateTrainingTrackingModel model);
        Task<ResponseModel<string>> DeleteTrainingTracking(long id);
        Task<ResponseModel<TrainingTrackingModel>> GetTrainingTrackingById(long id);
        Task<ResponseModel<List<TrainingTrackingModel>>> GetTrainingTrackings(int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<List<TrainingTrackingModel>>> GetTrainingTrackingsByCustomerId(long customerId, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<TrainingTrackingModel>> UpdateTrainingTracking(long id, CreateTrainingTrackingModel model);
    }
}