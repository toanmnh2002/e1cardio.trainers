﻿using E1.Cardio.BusinessObjects.Entities;
using E1.Cardio.Trainer.Libs.Models.AppModels;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace E1.Cardio.Trainer.Libs.Utils
{
    public static class JwtUtils
    {
        public static string Hash(this string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }
        public static bool Verify(this string input, string stringVerify)
        {
            return BCrypt.Net.BCrypt.Verify(input, stringVerify);
        }
        public static string GenerateToken(this Account account, Jwt jwt)
        {
            var role = account.GetRole();
            if (!string.IsNullOrEmpty(role))
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwt.SecretKey));
                var creds = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                        claims: new[] { new Claim("id", account.Id.ToString()),
                                        new Claim("phone", account.Phone),
                                        new Claim("role", role),
                                      },
                        expires: DateTime.UtcNow.AddDays(7),
                        signingCredentials: creds);

                return new JwtSecurityTokenHandler().WriteToken(token);
            }

            return "";
        }
        private static string GetRole(this Account account)
        {
            string role = "";
            if (account is Customer)
            {
                role = "Customer";
            }
            else if (account is BusinessObjects.Entities.Trainer)
            {
                role = "Trainer";
            }

            return role;
        }

    }
}
